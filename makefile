all: bin/cola

bin/cola: obj/main.o 
	gcc -Wall -fsanitize=address,undefined obj/main.o -o bin/cola

obj/main.o: src/main.c
	gcc -Wall -fsanitize=address,undefined  -c src/main.c -o obj/main.o


clean:
	rm obj/*.o bin/cola
